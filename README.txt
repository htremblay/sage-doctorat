-----------------------------
Description
-----------------------------

Ce dépôt contient tous les fichiers nécessaires à l'implémentation des algorithmes décrits dans la thèse de doctorat "Aspects algorithmiques de la géométrie discrète".

Les langages de programmation utilisés sont:
-Python
-C++

Les librairies utilisées sont:
-sage.combinat
-tikz
-LaTeX

----------------------------
factorisation
----------------------------

Ce dossier contient une implémentation des algorithmes présentés au chapitre 3. Elle nécessite le logiciel libre de calcul sage ainsi que le langage python afin d'être exécutée.

----------------------------
quadtree
----------------------------

Ce dossier contient une implémentation de la structure d'arbre quaternaire radix présentée au chapitre 4. Elle nécessite le langage python afin d'être exécutée. Mentionnons qu'elle a été développée conjointement avec Alexandre Blondin Massé.

----------------------------
outerhull-implementation
----------------------------

Ce dossier contient une implémentation de l'algorithme d'enveloppe externe présenté au chapitre 4. Elle nécessite un compilateur C++ afin d'être exécutée. Mentionnons qu'elle a été développée conjointement avec Martin Lavoie.
