﻿# -*- coding: utf-8 -*-
#*****************************************************************************
#       Copyright (C) 2015 Hugo Tremblay <tremblay.hugo.5@uqam.ca>,
#
#  Distributed under the terms of the GNU General Public License
#  version 3 (GPLv3)
#
#  The full text of the GPLv3 is available at:
#
#                  http://www.gnu.org/licenses/
#*****************************************************************************
r"""
Fonction de factorisation de polyominos sans trou.

AUTHORS:
    Hugo Tremblay
"""

F = WordPaths('0123', steps='square_grid')

def decode_bloc(w,b,k):
    r"""
    Décodage de w[i:i+b.length()] par le bloc b

    EXAMPLES::

        sage: w = Word("00100010")
        sage: b = Word("00")
        sage: decode_bloc(w,b,0)
        True
        sage: decode_bloc(w,b,1)
        False
    """
    for i in range(0,b.length()):
        if k+i > w.length()-1:
            return False
        if w[k+i]!=b[i]:
            return False
    return True

def hat(w):
    r"""
    Calcul de la fonction chapeau
    """

    h = WordMorphism('0->2,1->3,2->0,3->1')
    return F(h(w.reversal()))
    
def is_closed(w):
    h = 0
    v = 0
    for i in range(0,w.length()):
        if w[i] == '0':
            h = h+1
        if w[i] == '1':
            v = v+1
        if w[i] == '2':
            h = h-1
        if w[i] == '3':
            v = v-1
    return h == 0 and v == 0

def is_simple(w):
    for f in w.factor_iterator():
        if is_closed(f) and f != Word() and f != w:
            return False
    return True


def is_contour_word(w):
    if is_closed(F(w)) and is_simple(F(w)) and w != Word():
        return True
    else:
        return False
			

def fact_polyomino(w):
    r"""
    Retourne la factorisation du polyomino sans trou codé par le mot w
    
    EXAMPLES::
    	sage: fact_polyomino(Word('010010121232232303'))
    	w est composé
    	(word: 010, word: 121, word: 001223)
    """
    
    #Vérification que P est bien un mot de contour
    if is_contour_word(w) == False:
	print "P n'est pas un mot de contour"
	return w
		
    n = w.length()
    b = [Word(),Word(),Word(),Word()]

    for u in w.conjugates_iterator():
        if u[0]=='0':
        	
            #Parcours de tous les conjugués de w commençant par 0
            #----------------------------------------
            #print "--------------------------"
            #print "u = %s" %u
            #print "--------------------------"
            #----------------------------------------
            
            for i in range(0,n):
                #Construction de b0 et b2
                #print "i = "+str(i)
                #print "--------------"
                valide = True
                if u[i]=='0':
                    b[0] = u[0:i+1]
                    b[2] = hat(b[0])
                    c = 0
                    v = Word()
                    
                    #----------------------------------------
                    #print '- > b0 = %s    b2 = %s'%(b[0],b[2])
                    #----------------------------------------
                    
                    outofrange = False
                    #Décodage de b0 tant que c'est possible
                    while u[c]=='0' and valide and not outofrange:
                        #print 'c = '+str(c)
                        valide = decode_bloc(u,b[0],c)
                        v = v*Word('0')
                        if c+b[0].length() < n:
                            c = c+b[0].length()
                        else:
                            outofrange = True
                    if valide and not outofrange:
                    	
                    	#----------------------------------------
                    	#print '- > v = %s' %(v)
                    	#----------------------------------------
                    	
                        #Si toujours valide, alors construction de b1 et b3
                        for j in range(c,n):
                            m = v*Word('1')
                            outofrange = False
                            valide = True
                            if u[j]==u[c]:
                                b[int(u[c])] = u[c:j+1]
                                b[mod(int(u[c])+2,4)] = hat(b[int(u[c])])
                                
                                #----------------------------------------
                                #print ' ---- > b1 = %s    b3 = %s'%(b[1],b[3])
                                #----------------------------------------
                                
                                k = j+1
                                
                                #----------------------------------------
                                #print ' ---- > k = %s'%(k)
                                #----------------------------------------
                                
                                #On termine le décodage
                                while k<n and valide and not outofrange:
                                    #print "k = "+str(k)
                                    valide = decode_bloc(u,b[int(u[k])],k)
                                    m = m*Word(u[k])
                                    if k+b[int(u[k])].length() < n:
                                        k = k+b[int(u[k])].length()
                                    else:
                                        outofrange = True
                                phi = b[0]*b[1]*b[2]*b[3]
                                
                                #----------------------------------------
                                #print ' -------- > m = %s'%m
                                #print ' '
                                #----------------------------------------
                                
                                if (phi != Word('0123') and
                                    is_contour_word(phi) and
                                    m != Word('0123') and
                                    is_contour_word(m) and
                                    valide):
                                    print "w est composé"
                                    return (b[0],b[1],m)
    print "w est premier"
    return w
