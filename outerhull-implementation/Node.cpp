#include "Node.h"

unsigned int Node::nbNode = 0;
unsigned int Node::nbCallFindNeighbor = 0;
unsigned int Node::d = 0;
unsigned int Node::nbSons = 0;
unsigned int Node::nbNeighbors = 0;
unsigned int Son::d = 0;


Node * Node::findNeighbor(letter l,bool originalPath)
{
    //If the link already exist:  use it!
    Neighbors::iterator i = neighbors.find(l);

    if (i != neighbors.end() )
    {
        setNeighbor(l, i->second.first, originalPath);
        return i->second.first;
    }

    //Otherwise, ask the father... recursion might be involved.

    Son s = typeOfSon;
    int axe = l.axe();
    bool bit = s.bitValue(axe);
    s.flipBit(axe);

    Node * n = NULL;
    if ( (l.positive() && !bit) || (l.negative() &&  bit) ) {
        n = father->getSon(s);
    } else {
        n = father->findNeighbor(l,false)->getSon(s);
    }

    nbCallFindNeighbor++;

    setNeighbor(l, n, originalPath);
    return n;
}


