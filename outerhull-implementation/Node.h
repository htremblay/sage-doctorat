#ifndef NODE_H_
#define NODE_H_

#include <iostream>
#include <sstream>
#include <math.h>
#include <map>
#include <set>

using namespace std;


/**
 * Letters used for the Freeman chaincode
 *
 * the letter  n, n>0, means a translation by  e_n
 * the letter -n, n>0, means a translation by -e_n
 *
 */
class letter
{
    public:
        letter(int i) { l = i; }
        letter(const letter & other) {l = other.l;}
        ~letter(){}
        bool operator< (const letter & other) const {return l<other.l;}
        int intValue() const { return (l>0) ? (l-1)*2 : (-2*l)-1; }
        int freeman() const { return l>0 ? l-1: (-l)+1; }
        int axe() const { return intValue()/2;}
        bool positive() const { return l > 0;}
        bool negative() const { return l < 0;}
        friend ostream & operator << (ostream &, const letter &);
        int l;
};


class Son
{
    public :
        Son();
        Son (const Son & other);
        Son ( int value );
        ~Son();
        Son & operator=(const Son & other);
        bool operator<(const Son & other) const;
        bool operator==(const Son & other) const;
        Son & flipBit(int n);
        int  intValue() const;
        bool bitValue(int n) const;
        friend ostream & operator << (ostream &, const Son &);

    private:
        unsigned int val;
        static unsigned int d;

};


class Node 
{

    public:

        // Constructors

        // Empty constructor...
        Node();

        // Initialisatino by the father (normal initialisation type)
        Node(Node * father, const Son &);

        // Special initialisatino for the roots
        Node(const Son typeOfRoot);

        // Destructor
        ~Node();

        // Setters/Getters
        bool isVisited() const 
        { return visit_counter > 0;}

        void addVisit() 
        { visit_counter++; }

        void removeVisit()
        { if(isVisited()) visit_counter--; }

        void removeAllVisit()
        { visit_counter = 0; }

        void setNeighbor(letter l, Node * n, bool visitedPath=false) 
        { 
            auto it = neighbors.find(l);
            if(it == neighbors.end())
            {
                neighbors.insert(make_pair(l,make_pair(n,visitedPath)));
                n->neighbors.insert(make_pair(letter(-l.l),make_pair(this,visitedPath)));
            }
            else if(!it->second.second)
            {
                neighbors[l] = make_pair(n,visitedPath);
                n->neighbors[letter(-l.l)] = make_pair(this,visitedPath);
            }
            
            if(visitedPath)
            {
                passedNeighbors.insert(l);
                n->passedNeighbors.insert(letter(-l.l));
            }
        }


        // Principal method :
        // Returns a pointer to the appropriate neighbor.
        // If this neighbor does not exist in the tree, it is created.
        Node * findNeighbor(letter l,bool activePath=false);

        // For statistical purpose only
        static unsigned int nbNode;
        static unsigned int nbCallFindNeighbor;

        // In order to list the nodes, for visualisation of the tree
        void listNodes(stringstream & theVisited, 
                stringstream & theNonVisited) const;

        static void setDimension(int D)
        { d = D; nbSons = (int)pow(2,d); nbNeighbors = 2*d;}

        string printEtiquette() const;
        friend ostream & operator << (ostream &, const Node & n);

    private:
    public:
        static unsigned int d;
        //Son typeOfSon(const Node *) const;
        Node * getSon(const Son s);
        Node * father;
        int visit_counter;
        Son typeOfSon;

        typedef pair<Node*,bool> NeighborsInfo;
        typedef map<letter,NeighborsInfo> Neighbors;
        Neighbors neighbors;

        typedef set<letter> PassedNeighbors;
        PassedNeighbors passedNeighbors;

        typedef map<Son, Node*> Sons;
        Sons sons;

        static unsigned int nbSons;
        static unsigned int nbNeighbors;

        // For visualisation
        void listNodesRecursively(stringstream & theVisited, 
                stringstream & theNonVisited) const;

    private:
        // Tentative pour optimiser en allouant beaucoup de mémoire d'un
        // coup
        static Node * getEmptyNode();
        void initNode(Node * f, const Son & s);

};

#include "Node.ih"

#endif
