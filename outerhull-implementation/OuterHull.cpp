#include <vector>
#include <algorithm>
#include <iostream>
#include <chrono>
#include <iterator>
#include <list>
#include <set>
#include "Tree.h"

using namespace std;

letter Step(Node* c, Node* W)
{
#ifdef print_debug
    cout << "c:" << c << endl << "w:" << W <<endl;
#endif

    for(auto v: c->neighbors)
    {
        if(v.second.first == W)
        {
            return v.first;
        }
    }
    return letter(0);
}

void ShowNeighbor(Node* c)
{
#ifdef print_debug
    cout << "-------------" << endl;
    cout << "node: " << c << endl;
    cout << "Neighbor visited" << endl;
    for(auto v: c->neighbors)
    {
        cout << v.first.l << ": " << v.second.second << " (" << v.second.first << ")" << endl;
    }
    cout << "Set neighbor" << endl;
    for(auto v: c->passedNeighbors)
    {
        cout << v.l << endl;
    }
    cout << "-------------" << endl;
#endif
}


void OuterHull(Tree& t, list<letter>& wp)
{
    Node* w = t.getRoot();
    std::set<Node*> n;
    for(auto it = w->neighbors.begin(); it != w->neighbors.end(); it++)
    {
        if(it->second.second)
        {
            n.insert(it->second.first);
        }
    }
    Node* c = nullptr;

    int initialDirection = w->passedNeighbors.find(letter(1)) != w->passedNeighbors.end()? 1 : 2;
    c = w->neighbors.find(letter(initialDirection))->second.first;
    
    wp.push_back(Step(w,c));

#ifdef print_debug

    cout << "--+--+--+--+--+--+--+--+--" << endl;
    cout << "Detail current active" << endl;
    ShowNeighbor(t.getActive());

    cout << "--+--+--+--+--+--+--+--+--" << endl;
    cout << "next: " << c << endl;
    cout << "step: " << Step(w,c).l << endl;
    int i = 0;
#endif
    while(c != w || !n.empty())
    {
#ifdef print_debug
        cout << "===========================================================================" << endl;
        cout << "iteration: " << i++ << endl;
#endif
        int turn = 2;
        Node* next = nullptr;
        ShowNeighbor(c);
        for(auto v: c->neighbors)
        {
            if(v.second.second)
            {
                auto vr = v.second.first;
                int virage = Step(c,vr).freeman()-wp.back().freeman();
#ifdef print_debug
                cout << "Virage: " << virage << endl;
#endif 

                ShowNeighbor(vr);
                if((virage + 5) % 4 <= (turn + 5) % 4)
                {
#ifdef print_debug
                    cout << "Switch next" << endl;
#endif
                    turn = virage;
                    next = vr;
                }
            }
        }
#ifdef print_debug
        cout << "--+--+--+--+--+--+--+--+--" << endl;
        cout << "next: " << next << endl;
        cout << "turn: " << turn << endl;
        cout << "step: " << Step(c,next).l << endl;
#endif
        wp.push_back(Step(c,next));
        n.erase(c);
        c = next;
    }
}


int main(int argc, char** argv)
{
    list<letter> wp;

    auto start = std::chrono::steady_clock::now();
    Tree t(2);
    for_each(istream_iterator<int>(cin), istream_iterator<int>(),
            [&t](int i)
            {
#ifdef print_debug
                cout << "+++++++++++++++++++++++++++++++++++++++++++" << endl;
                cout << i << endl;
                Node* active = t.getActive();
                ShowNeighbor(active);
#endif
                t.nextStep(i);
#ifdef print_debug
                ShowNeighbor(active);
#endif
            });

    OuterHull(t,wp);
    auto end = std::chrono::steady_clock::now();

    for(auto e: wp)
        cout << e.freeman() << " ";

    cout << endl;

    cerr << "Execution time (nanoseconds): " << std::chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << endl;
    cerr << "Execution time (milliseconds): " << std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count() << endl;
#ifdef print_debug
    cerr << "Node::nbSons: " << Node::nbSons << endl << "Node::nbNeighbors: " << Node::nbNeighbors << endl;
#endif 

    return 0;
}
