#ifndef TREE_H_
#define TREE_H_

#include <iostream>
#include <sstream>
#include "Node.h"

using namespace std;


class Tree {

	public:


		// Constructors
		Tree(unsigned int dim = 0);

		// Destructor
		~Tree();


		//Main method : nextStep
		//
		// Transfer the active node to the neighbor of the actual 
		// active node and mark it as visited.
		// Returns true if the path is not self intersecting.
		// Returns false if the path crosses itself at this point.
		//
		// Reading type letter 
		bool nextStep(letter);
		bool nextStep(int);

		void listNodes(stringstream & theVisited, 
				stringstream & theNonVisited);

		static unsigned int nbCallFindNeighbor;

		friend ostream & operator << (ostream & , Tree & );

		static void setDimension(unsigned int);

        Node* getActive();
        Node* getRoot();

	private:
	public:
		static unsigned int d;
		Node ** roots;
		Node * active;
};


#include "Tree.ih"

#endif
