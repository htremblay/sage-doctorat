import sys
from discrete_path import Multipaths

def example(number):
    p = Multipaths(2)
    if number == 1:
        p.write_path('rrrruuuulllldddd', 0)
        p.translate_to(1, 1)
        p.write_path('rruulldd', 1)
    elif number == 2:
        p.write_path('rrrruuullllddd', 0)
        p.translate_to(1, 1)
        p.write_path('rruuldld', 1)
    elif number == 3:
        p.write_path('rrrruulllldd', 0)
        p.write_path('rrruuldlurullddd', 1)
    elif number == 4:
        p.write_path('rrruuulllddd', 0)
        p.write_path('rrruuldlurullddd', 1)
    elif number == 5:
        p.write_path('rrruuldluldd', 0)
        p.translate_to(0, 1)
        p.write_path('rurdruullldd', 1)
    elif number == 6:
        p.write_path('rrruuuuulddddluulddd', 0)
        p.translate_to(0, 2)
        p.write_path('rurrrulllulddd', 1)
    elif number == 7:
        p.write_path('rrruulluurullddddd', 0)
        p.translate_to(2, 0)
        p.write_path('rruuuuulldddrdld', 1)
    elif number == 8:
        p.write_path('rruuruluullddddd', 0)
        p.translate_to(2, 0)
        p.write_path('rruuuuullddddd', 1)
    elif number == 9:
        p.write_path('rrruuulllddd', 0)
        p.translate_to(2, 2)
        p.write_path('rrruuulllddd', 1)
    elif number == 10:
        p.write_path('rrruuulllddd', 0)
        p.translate_to(3, 3)
        p.write_path('rrruuulllddd', 1)
    elif number == 11:
        p.write_path('rrrrrruuuullddlluurruulllldddddd', 0)
        p.translate_to(3,1)
        p.write_path('rrrruuuuuullllllddddrruurrddlldd', 1)
    elif number == 12:
        p.write_path('rrrrruuuuullllldrrrrdddlllld', 0)
        p.translate_to(2,2)
        p.write_path('ruld', 1)
    return p

# Testing
if len(sys.argv) >= 2:
    number = int(sys.argv[1])
else:
    number = 10 
print example(number).tikz(plain=False)
