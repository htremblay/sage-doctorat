import math

r"""
Useful functions
"""

RIGHT = ( 1, 0)
LEFT  = (-1, 0)
UP    = ( 0, 1)
DOWN  = ( 0,-1)

def is_origin(x, y):
    return x == 0 and y == 0

def is_on_axis(x, y):
    return x == 0 or y == 0

def sign(x):
    if x == 0:
        return 0
    elif x > 0:
        return 1
    else:
        return -1

def parent(x):
    return sign(x) * (abs(x) / 2)

def share_parent(x1, y1, x2, y2):
    r"""
    Returns True if and only if points `(x1,y1)` and `(x2,y2)` have the same
    parent.
    """
    return abs(x1) / 2 == abs(x2) / 2 and abs(y1) / 2 == abs(y2) / 2

# Tikz writing
# Regions writing
def regions_iter(xmin, ymin, xmax, ymax):
    assert xmin % 2 == 1 and ymin % 2 == 1 and xmax % 2 == 1 and ymax % 2 == 1
    yield (-1,-1,1,1,0)
    for x in range(-2, xmin, -2): yield (x-1,-1,x,1,x.bit_length())
    for x in range(2, xmax, 2):   yield (x,-1,x+1,1,x.bit_length())
    for y in range(-2, ymin, -2): yield (-1,y-1,1,y,y.bit_length())
    for y in range(2, ymax, 2):   yield (-1,y,1,y+1,y.bit_length())
    for x in range(xmin, xmax + 1):
        for y in range(ymin, ymax + 1):
            if x not in [-1, 0, 1] and y not in [-1, 0, 1] and\
               abs(x) / 2 == (abs(x + 1)) / 2 and\
               abs(y) / 2 == (abs(y + 1)) / 2:
                level = min(max(a.bit_length(), b.bit_length()) for (a,b) in [(x,y),(x+1,y),(x,y+1),(x+1,y+1)])
                yield (x,y,x+1,y+1,level)

def regions(xmin, ymin, xmax, ymax):
    s = ''
    offset = 0.4
    for (a,b,c,d,l) in regions_iter(xmin, ymin, xmax, ymax):
        s += r'  \draw[region, fill=blue!5] (%s-%s,%s-%s) rectangle (%s+%s,%s+%s);' %\
                (a,offset,b,offset,c,offset,d,offset)
        s += '\n'
    return s

def shift(direction, color, number_of_colors):
    space = 0.05
    offset = (number_of_colors - 1) * space / 2
    if direction[0] == 0:
        return (color * space - offset, 0)
    else:
        return (0, color * space - offset)

def get_tikz_bounding_box(bounding_box):
    (xmin, ymin, xmax, ymax) = bounding_box
    xmin = xmin - xmin % 2 - 1
    xmax = xmax + xmax % 2 + 1
    ymin = ymin - ymin % 2 - 1
    ymax = ymax + ymax % 2 + 1
    return (xmin, ymin, xmax, ymax)

class Node:
    r"""
    Nodes in the data structure
    """

    def __init__(self, parent, number_of_colors):
        self.parent = parent
        self.children = {}
        self.neighbors = {}
        self.edge_color = {}

    def __repr__(self):
        return 'Node\n Children: %s\n Neighbors: %s' %\
               (self.children.keys(), self.neighbors.keys())

    def tikz(self, colormap, x, y, visited):
        s = ''
        if (x,y) not in visited:
            visited.add((x,y))
            for (direction,neighbor) in self.neighbors.items():
                xp = x + direction[0]
                yp = y + direction[1]
                s += neighbor.tikz(colormap, xp, yp, visited)
                if direction in self.edge_color:
                    number_of_colors = len(self.edge_color[direction])
                    lwidth = 1.3 / number_of_colors
                    for (i,(color,edge_direction)) in enumerate(sorted(self.edge_color[direction])):
                        if edge_direction == '+':
                            arrow = "-latex', shorten >=2mm"
                        elif edge_direction == '-':
                            arrow = "latex'-, shorten <=2mm"
                        else:
                            arrow = "latex'-latex', shorten <=2mm, shorten >=2mm"
                        style = '%s, draw=%s, line width=%smm' %\
                                (arrow, colormap[color], lwidth)
                        s += r'  \draw[%s] (%s,%s) ++ %s -- ++ (%s,%s);' % (style, x, y, shift(direction, i, number_of_colors), direction[0], direction[1])
                else:
                    style = 'draw=black, dashed, very thick'
                    s += r'  \draw[%s] (%s,%s) -- (%s,%s);' % (style, x, y, xp, yp)
                s += '\n'
                s += r'  \node[point] at (%s,%s) {};' % (x, y)
                s += '\n'
                s += r'  \node[point] at (%s,%s) {};' % (xp, yp)
                s += '\n'
        return s

    def get_child(self, i, j):
        if (i,j) not in self.children:
            self.children[(i,j)] = Node(self, len(self.edge_color))
        return self.children[(i,j)]

    def get_neighbor(self, x, y, dx, dy):
        if (dx, dy) not in self.neighbors:
            xp = x + dx
            yp = y + dy
            if is_origin(x, y):
                neighbor = self.get_child(dx, dy)
            elif is_origin(xp, yp):
                neighbor = self.parent
            else:
                i = -1 if xp == -1 else xp % 2
                j = -1 if yp == -1 else yp % 2
                if share_parent(x, y, xp, yp):
                    neighbor = self.parent.get_child(i, j)
                else:
                    neighbor = self.parent.get_neighbor(parent(x), parent(y), dx, dy).get_child(i, j)
            self.neighbors[(dx,dy)] = neighbor
            neighbor.neighbors[(-dx,-dy)] = self
        return self.neighbors[(dx,dy)]

    def set_edge_color(self, x, y, dx, dy, color):
        neighbor = self.get_neighbor(x, y, dx, dy)
        if (dx,dy) not in self.edge_color:
            self.edge_color[(dx,dy)] = set()
        if (color,'-') in self.edge_color[(dx,dy)]:
            self.edge_color[(dx,dy)] -= set([(color, '-'), (color, '+')])
            self.edge_color[(dx,dy)].add((color, '--'))
        elif (color, '--') not in self.edge_color[(dx,dy)]:
            self.edge_color[(dx,dy)].add((color, '+'))
        if (-dx,-dy) not in neighbor.edge_color:
            neighbor.edge_color[(-dx,-dy)] = set()
        if (color,'+') in neighbor.edge_color[(-dx,-dy)]:
            neighbor.edge_color[(-dx,-dy)] -= set([(color, '-'), (color, '+')])
            neighbor.edge_color[(-dx,-dy)].add((color, '--'))
        elif (color, '--') not in neighbor.edge_color[(-dx,-dy)]:
            neighbor.edge_color[(-dx,-dy)].add((color, '-'))

class Multipaths:
    r"""
    Multiple discrete path
    """

    def __init__(self, number_of_colors):
        self.number_of_colors = number_of_colors
        self.point = (0,0)
        self.root = Node(None, number_of_colors)
        self.current_node = self.root
        self.bounding_box = [0, 0, 0, 0]

    def __repr__(self):
        return 'Multiple paths with colors %s and bounding box %s' %\
                (range(self.number_of_colors), self.bounding_box)

    def tikz(self, colormap={0: 'blue', 1: 'red', 2: 'orange'}, bounding_box=None, plain=True):
        s = ''
        if not plain:
            s += r'\begin{tikzpicture}[scale=2, grille/.style={dotted, thin}, point/.style={circle, minimum size=4mm, fill=black, inner sep=0pt}, region/.style={rounded corners, dashed, very thick, draw=blue!50}]'
        s += '\n'
        if bounding_box is None:
            bounding_box = get_tikz_bounding_box(self.bounding_box)
        s += regions(*bounding_box)
        s += '\n'
        s += r'  \draw[grille] (%s,%s) grid (%s,%s);' % bounding_box
        s += '\n'
        visited = set()
        s += self.root.tikz(colormap, 0, 0, visited)
        if not plain:
            s += r'\end{tikzpicture}'
        return s

    def translate_to(self, x, y):
        while self.point != (x,y):
            if self.point[0] < x:
                self._translate_to((1,0))
            elif self.point[0] > x:
                self._translate_to((-1,0))
            elif self.point[1] < y:
                self._translate_to((0,1))
            elif self.point[1] > y:
                self._translate_to((0,-1))

    def write_path(self, word, color):
        for letter in word:
            self.write_edge(letter, color)

    def write_edge(self, letter, color):
        if letter == 'r':
            (dx,dy) = (1,0)
        elif letter == 'u':
            (dx,dy) = (0,1)
        elif letter == 'l':
            (dx,dy) = (-1,0)
        elif letter == 'd':
            (dx,dy) = (0,-1)
        neighbor = self.current_node.get_neighbor(self.point[0],\
                                                  self.point[1],\
                                                  dx,\
                                                  dy)
        self.current_node.set_edge_color(self.point[0],\
                                         self.point[1],\
                                         dx,\
                                         dy,\
                                         color)
        self.point = (self.point[0] + dx,\
                      self.point[1] + dy)
        self._update_bounding_box()
        self.current_node = neighbor

    def _update_bounding_box(self):
        self.bounding_box[0] = min(self.bounding_box[0], self.point[0])
        self.bounding_box[1] = min(self.bounding_box[1], self.point[1])
        self.bounding_box[2] = max(self.bounding_box[2], self.point[0])
        self.bounding_box[3] = max(self.bounding_box[3], self.point[1])

    def _translate_to(self, direction):
        neighbor = self.current_node.get_neighbor(self.point[0],\
                                                  self.point[1],\
                                                  direction[0],\
                                                  direction[1])
        self.point = (self.point[0] + direction[0],\
                      self.point[1] + direction[1])
        self.current_node = neighbor

    def walk(self, letter):
        pass

    def is_intersection_point(self):
        pass

    def delete_incoming_arc(self, letter, color):
        pass

    def has_outgoing_arc(self, letter, color):
        pass

    def outgoing_colors(self, letter):
        pass

    def has_incoming_arc(self, letter, color):
        pass

    def incoming_colors(self, letter):
        pass

