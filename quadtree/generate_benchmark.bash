for i in {1..12}
do
	filename=exemple-$i.tex
	python benchmark.py $i > $filename
	./tikz2pdf.py $filename
	rm -f $filename *.aux *.log
done
pdftk exemple-*.pdf cat output benchmark.pdf
