import Queue
from discrete_path import Multipaths

r"""
Create the overlay of n discrete paths
"""

###############################
#Paths initialization
###############################

def set_multipaths(paths):

    nb_paths = len(paths)
    m = Multipaths(nb_paths)

    for i in range(nb_paths):
        m.write_path(paths[i],i)

    return m

######################################
#Queue initialization and creation
######################################

def set_queue(multi_paths):
    
    q = Queue.Queue(maxsize=0)

    q.put(multi_paths.current_node)

    while not q.empty():
        print q.get()

t = set_multipaths(['rruulldd','rrruullldd'])
set_queue(t)
